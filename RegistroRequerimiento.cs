﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ene
{
    public partial class RegistroRequerimiento : Form
    {
        Empleado empleado = new Empleado();
        List<Empleado> empleadosList = new List<Empleado>();
        Requerimiento requerimiento = new Requerimiento();
        List<Requerimiento> requerimientos = new List<Requerimiento>();
        TipoRequerimiento tipoRequerimientos = new TipoRequerimiento();
        List<TipoRequerimiento> tipoRequerimientosList = new List<TipoRequerimiento>();
        SqlConnection connection;
        public RegistroRequerimiento()
        {
            InitializeComponent();
            this.connection = new SqlConnection("SERVER=DESKTOP-44EITSK;DATABASE=Ene;integrated security=true");
            this.connection.Open();
            addTipoRequerimientoToCombobox();
            addUsuarioToCombobox();
        }

        private void addTipoRequerimientoToCombobox()
        {
            String queryTipo = "select * from tipo_requerimiento";
            SqlCommand cmd = new SqlCommand(queryTipo, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.tipoRequerimientos = new TipoRequerimiento();
                this.tipoRequerimientos.Id = Convert.ToInt32(reader["id"]);
                this.tipoRequerimientos.Nombre = Convert.ToString(reader["nombre"]);
                Console.WriteLine(this.tipoRequerimientos.Nombre);
                this.tipoRequerimientosList.Add(this.tipoRequerimientos);
                requerimientoBox.Items.Add(this.tipoRequerimientos.Nombre);

            }
            reader.Close();
        }

        private void addUsuarioToCombobox()
        {
            String queryEmpleado = "Select * from empleado";
            SqlCommand cmd = new SqlCommand(queryEmpleado, this.connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.empleado.Id = Convert.ToInt32(reader["id"]);
                this.empleado.Name = Convert.ToString(reader["name"]);
                this.empleado.Email = Convert.ToString(reader["email"]);
                this.empleado.Password = Convert.ToString(reader["password"]);
                userBox.Items.Add(this.empleado.Name);
                this.empleadosList.Add(this.empleado);
            }
            reader.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            requerimientoBox.SelectedItem = null;
            userBox.SelectedItem = null;
            txtDescripcion.Text = "";
            priorityBox.Text = null;
            this.requerimiento = new Requerimiento();

        }
        private void saveRequerimiento(Requerimiento saveRequerimiento) {
            string query = "INSERT INTO requerimiento (nombre,descripcion,prioridad,idUsuario,idRequerimiento,status,plazo) values (@nombre,@descripcion,@prioridad,@idUsuario,@idTipoRequerimiento,@status,@plazo)";
            SqlCommand command = new SqlCommand(query,this.connection);

            command.Parameters.AddWithValue("@nombre", "prueba");
            command.Parameters.AddWithValue("@descripcion", saveRequerimiento.Descripcion);
            command.Parameters.AddWithValue("@prioridad", saveRequerimiento.Prioridad);
            command.Parameters.AddWithValue("@idUsuario", 1);//TODO cambiar a id usuario correspondiente
            command.Parameters.AddWithValue("@idTipoRequerimiento", saveRequerimiento.Id_tipo_requerimiento);
            command.Parameters.AddWithValue("@status",0);
            switch (saveRequerimiento.Prioridad) {
                case 1:
                    command.Parameters.AddWithValue("@plazo", 3);
                    break;
                case 2:
                    command.Parameters.AddWithValue("@plazo", 4);
                    break;
                case 3:
                    command.Parameters.AddWithValue("@plazo", 5);
                    break;
                default:
                    MessageBox.Show("Prioridad ingresada no es valida");
                    break;
            }
            Console.WriteLine(saveRequerimiento.Id_tipo_requerimiento);
            command.ExecuteNonQuery();
            MessageBox.Show("El requerimiento se ha guardado correctamente");
        }

        private void RegistroRequerimiento_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (validarForm()) {
                this.requerimiento.Descripcion = txtDescripcion.Text;
                this.requerimiento.Prioridad = Int32.Parse(priorityBox.SelectedItem.ToString());
                for (var i = 0; i < empleadosList.Count; i++)
                {
                    if (userBox.SelectedItem.ToString().Equals(empleadosList[i].Name))
                    {
                        this.requerimiento.Empleado = empleadosList[i];
                    }
                }
                for (var i = 0; i < tipoRequerimientosList.Count; i++)
                {
                    if (requerimientoBox.SelectedItem.ToString().Equals(tipoRequerimientosList[i].Nombre))
                    {
                        this.requerimiento.Id_tipo_requerimiento = tipoRequerimientosList[i].Id;
                    }
                }
                saveRequerimiento(this.requerimiento);
            }
        }

        private bool validarForm() {
            if (txtDescripcion.Text == "" | priorityBox.SelectedItem == null | txtDescripcion.Text == "" | requerimientoBox.SelectedItem == null | userBox.SelectedItem == null) {
                MessageBox.Show("Ingrese los datos necesarios");
                return false;
            }
            return true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ListarRequerimiento listar = new ListarRequerimiento();
            listar.Show();
            
        }
    }
}
