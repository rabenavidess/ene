﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ene
{
    class Requerimiento
    {
        private int id;
        private string nombre;
        private int prioridad;
        private string tipo_requerimiento;
        private int id_tipo_requerimiento;
        private string descripcion;
        private Empleado empleado;
        private int diasPlazos;




        public Requerimiento() { }

        public Requerimiento(int id, string nombre, int prioridad, string tipo_requerimiento, int id_tipo_requerimiento, string descripcion, Empleado empleado)
        {
            this.id = id;
            this.nombre = nombre;
            this.prioridad = prioridad;
            this.tipo_requerimiento = tipo_requerimiento;
            this.id_tipo_requerimiento = id_tipo_requerimiento;
            this.descripcion = descripcion;
            this.empleado = empleado;
        }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Prioridad { get => prioridad; set => prioridad = value; }
        public string Tipo_requerimiento { get => tipo_requerimiento; set => tipo_requerimiento = value; }
        public int Id_tipo_requerimiento { get => id_tipo_requerimiento; set => id_tipo_requerimiento = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int DiasPlazos { get => diasPlazos; set => diasPlazos = value; }
        internal Empleado Empleado { get => empleado; set => empleado = value; }
    }
}
