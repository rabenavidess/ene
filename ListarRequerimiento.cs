﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ene
{
    public partial class ListarRequerimiento : Form
    {
        TipoRequerimiento tipoRequerimientos = new TipoRequerimiento();
        List<TipoRequerimiento> tipoRequerimientosList = new List<TipoRequerimiento>();
        SqlConnection connection;
        Requerimiento requerimiento = new Requerimiento();
        List<Requerimiento> requerimientos = new List<Requerimiento>();

        public ListarRequerimiento()
        {
            InitializeComponent();
            this.connection = new SqlConnection("SERVER=DESKTOP-44EITSK;DATABASE=Ene;integrated security=true");
            this.connection.Open();
            addTipoRequerimientoToCombobox();
        }    
            


        private void addTipoRequerimientoToCombobox()
        {
            String queryTipo = "select * from tipo_requerimiento";
            SqlCommand cmd = new SqlCommand(queryTipo, this.connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.tipoRequerimientos = new TipoRequerimiento();
                this.tipoRequerimientos.Id = Convert.ToInt32(reader["id"]);
                this.tipoRequerimientos.Nombre = Convert.ToString(reader["nombre"]);
                Console.WriteLine(this.tipoRequerimientos.Nombre);
                this.tipoRequerimientosList.Add(this.tipoRequerimientos);
                requerimientoBox.Items.Add(this.tipoRequerimientos.Nombre);

            }
            reader.Close();
        }


        private void Login_Click(object sender, EventArgs e)
        {

        }

        private void ListarRequerimiento_Load(object sender, EventArgs e)
        {

        }

        private void requerimientoBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.requerimientos = new List<Requerimiento>();
            int idTipoRequerimiento = 0;

            if (requerimientoBox.SelectedItem == null)
            {
                MessageBox.Show("Ingrese el tipo de requerimiento");
                return;
            }

            if (prioridadBox.SelectedItem == null)
            {
                MessageBox.Show("Ingrese la prioridad del requerimiento");
                return;
            }

            for (var i = 0; i < this.tipoRequerimientosList.Count; i++)
            {
                if (this.tipoRequerimientosList[i].Nombre.Equals(requerimientoBox.SelectedItem.ToString()))
                {
                    idTipoRequerimiento = this.tipoRequerimientosList[i].Id;
                }
            }

            String queryTipo = "select * from requerimiento where prioridad = " + prioridadBox.SelectedItem.ToString() + " and idRequerimiento = "+ idTipoRequerimiento;
            if (radioButton1.Checked) {
                queryTipo += "and status = 0";
            }

            if (radioButton2.Checked) {
                queryTipo += "and status = 1";
            }

            SqlCommand cmd = new SqlCommand(queryTipo, this.connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.requerimiento = new Requerimiento();
                this.requerimiento.Id = Convert.ToInt32(reader["id"]);
                this.requerimiento.Descripcion = Convert.ToString(reader["descripcion"]);
                this.requerimiento.Prioridad = Convert.ToInt32(reader["prioridad"]);
                this.requerimiento.DiasPlazos = Convert.ToInt32(reader["plazo"]);
                this.requerimiento.Id_tipo_requerimiento = Convert.ToInt32(reader["idRequerimiento"]);
                this.requerimientos.Add(this.requerimiento);
            }
            reader.Close();
            String queryTipoRequerimient = "select * from tipo_requerimiento";
            SqlCommand command = new SqlCommand(queryTipoRequerimient, this.connection);
            SqlDataReader data = command.ExecuteReader();

            while (data.Read())
            {
                for (var i = 0; i < this.requerimientos.Count; i++)
                {
                    if (this.requerimientos[i].Id_tipo_requerimiento == Convert.ToInt32(data["id"])) {
                        this.requerimientos[i].Tipo_requerimiento = Convert.ToString(data["nombre"]);
                    }
                }
            }
            data.Close();
            llenarTabla();
        }

        private void llenarTabla() {

            dataRequerimiento.Rows.Clear();
            for (var i = 0; i < this.requerimientos.Count; i++) {
                dataRequerimiento.Rows.Add(this.requerimientos[i].Tipo_requerimiento, this.requerimientos[i].Prioridad,
                                            this.requerimientos[i].Descripcion, this.requerimientos[i].DiasPlazos,this.requerimientos[i].Id);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string query = "update requerimiento set status = 1 where id = ";
            modificarEliminar(query);
            MessageBox.Show("El requerimiento se ha guardo correctamente");
            button1_Click(new object(),new EventArgs());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = "delete from requerimiento where id = ";
            modificarEliminar(query);
            MessageBox.Show("El requerimiento se ha eliminado correctamente");
            button1_Click(new object(), new EventArgs());
        }

        private void modificarEliminar(string query) {
            Int32 selectedRowCount = dataRequerimiento.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {

                for (int i = 0; i < selectedRowCount; i++)
                {
                    string data = dataRequerimiento.SelectedRows[i].Cells[4].Value.ToString();
                    query += data;
                    SqlCommand command = new SqlCommand(query, this.connection);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
