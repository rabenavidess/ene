﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ene
{
    class TipoRequerimiento
    {
        private int id;
        private string nombre;

        public TipoRequerimiento(int id, string nombre)
        {
            this.Id = id;
            this.Nombre = nombre;
        }
        public TipoRequerimiento() { }
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
